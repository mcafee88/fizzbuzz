function fizzbuzz(maxValue) {
    let jog = ''
    for (let run = 1; run <= maxValue; run++) {
        if (run % 2 === 0 && run % 3 === 0) jog += 'fizzbuzz,'
        else if (run % 2 === 0) jog += 'fizz,'
        else if (run % 3 === 0) jog += 'buzz,'
        else jog += run + ","

    }
    return jog
}
console.log(fizzbuzz(10))